package org.webrtc.webrtcdemo;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.ContentResolver;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;

import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class LoginActivity extends Activity implements LoaderCallbacks<Cursor> {

    private UserLoginTask mAuthTask = null;

    // UI references.
    public AutoCompleteTextView mEmailView;
    public EditText mPasswordView;
    public View mProgressView;
    public View mLoginFormView;
    public Context loginContext;
    public static Socket      mSocket;
    public static PrintWriter out;
    public static BufferedReader in;
    public static JSONObject friendList,lastChat,lastChatDate,userIP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Set up the login form.
        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);
        populateAutoComplete();

        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "登入中...", Toast.LENGTH_SHORT).show();
                attemptLogin();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
        loginContext=getApplicationContext();
    }

    private void populateAutoComplete() {
        getLoaderManager().initLoader(0, null, this);
    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    public void attemptLogin() {
        try{//another thread,

            JSONArray clientRequest= new JSONArray();
            clientRequest.put(0,0);
            clientRequest.put(1,mEmailView.getText().toString());
            clientRequest.put(2,mPasswordView.getText().toString());

             mAuthTask = new UserLoginTask(clientRequest.toString(),loginContext);
             mAuthTask.execute((Void) null);

            return;
        } catch (JSONException e) {e.printStackTrace();}

    }



    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(this,
                // Retrieve data rows for the device user's 'profile' contact.
                Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
                        ContactsContract.Contacts.Data.CONTENT_DIRECTORY), ProfileQuery.PROJECTION,

                // Select only email addresses.
                ContactsContract.Contacts.Data.MIMETYPE +
                        " = ?", new String[]{ContactsContract.CommonDataKinds.Email
                .CONTENT_ITEM_TYPE},

                // Show primary email addresses first. Note that there won't be
                // a primary email address if the user hasn't specified one.
                ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        List<String> emails = new ArrayList<String>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            emails.add(cursor.getString(ProfileQuery.ADDRESS));
            cursor.moveToNext();
        }

        addEmailsToAutoComplete(emails);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };

        int ADDRESS = 0;
        int IS_PRIMARY = 1;
    }


    private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
        ArrayAdapter<String> adapter =
                new ArrayAdapter<String>(LoginActivity.this,
                        android.R.layout.simple_dropdown_item_1line, emailAddressCollection);

        mEmailView.setAdapter(adapter);
    }


    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

        private String      userRequestJSONStr;
        private JSONArray   serverResponseJSONArr;
        public Context parentLoginContext;


        UserLoginTask(String userRequestJSONString,Context loginContext) {
            userRequestJSONStr=userRequestJSONString;
            parentLoginContext=loginContext;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                String hostName="192.168.0.4";
                int portNumber=5000;
                mSocket = new Socket(hostName, portNumber);
                out = new PrintWriter(mSocket.getOutputStream(), true);
                in = new BufferedReader(new InputStreamReader(mSocket.getInputStream()));

                out.println(userRequestJSONStr);
                String serverResponse;
                while (((serverResponse=in.readLine()) != null)) {break;}
                Log.d("Junhao","JSONObject"+serverResponse);
                JSONObject serverResponseJSONObj=new JSONObject(serverResponse);

                //if server reply valid authentication
                if(serverResponseJSONObj.getInt("0")==10 && serverResponseJSONObj.getInt("1")==1){
                    Log.d("Junhao","Authenticated by Server");

                    friendList=new JSONObject(serverResponseJSONObj.getString("2"));
                    lastChat=new JSONObject(serverResponseJSONObj.getString("3"));
                    lastChatDate=new JSONObject(serverResponseJSONObj.getString("4"));
                    userIP=new JSONObject(serverResponseJSONObj.getString("5"));//list of online user ip

                    return true;
                }else{
                    Log.d("Junhao","Authentication failed");
                    return false;//server close the port
                }
            }catch(IOException ioe){
                Log.d("Junhao","IOException:doInBackground");
                return false;}
            catch(JSONException je){
                Log.d("Junhao","JSONException:doInBackground");
                return false;
            }


        }

        @Override
        protected void onPostExecute(final Boolean success) {//success is a boolean returned by doinbackground
            if(success){
                Log.d("Junhao","Starting new activity");
                startActivity(new Intent(LoginActivity.this, WebRTCDemo.class));
            }else{
                Log.d("Junhao","onPostExecute:failed");
                mEmailView.setError(getString(R.string.error_incorrect_password));
                mPasswordView.setError(getString(R.string.error_incorrect_password));
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }
}

