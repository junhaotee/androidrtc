package org.webrtc.webrtcdemo;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class LazyAdapter extends BaseAdapter {
    
    private Activity activity;
    private ArrayList data;
    private static LayoutInflater inflater=null;
    public ImageLoader imageLoader;

    public LazyAdapter(Activity a, ArrayList d) {
        activity = a;
        data=d;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        imageLoader=new ImageLoader(activity.getApplicationContext());
    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }
    
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        if(convertView==null)
            vi = inflater.inflate(R.layout.item, null);

        TextView username=(TextView)vi.findViewById(R.id.username);;
        ImageView image=(ImageView)vi.findViewById(R.id.image);

        username.setText(data.get(position).toString());

        TextView userid=(TextView)vi.findViewById(R.id.userid);
        userid.setText(FriendsFragment.friendArrListId.get(position));

        //chatdialogue,lastchatedate,onlinestat

        //imageLoader.DisplayImage(data.get(position).toString(), image);
        imageLoader.DisplayImage("http://junhao.my/avatar/" + FriendsFragment.friendArrListId.get(position) + ".jpg", image);
        return vi;
    }
}