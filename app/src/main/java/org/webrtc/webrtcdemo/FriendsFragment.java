/*
 *  Copyright (c) 2013 The WebRTC project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree. An additional intellectual property rights grant can be found
 *  in the file PATENTS.  All contributing project authors may
 *  be found in the AUTHORS file in the root of the source tree.
 */

package org.webrtc.webrtcdemo;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;

public class FriendsFragment extends Fragment {

  private String TAG;
  private MenuStateProvider stateProvider;

    public static ListView friendListView;
    public static LazyAdapter adapter;
    public static Activity FriendsFragmentActivity;
    public static ArrayList<String> friendArrList;
    public static ArrayList<String> friendArrListId;

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

        final View v = inflater.inflate(R.layout.settingsmenu, container, false);
        TAG = getResources().getString(R.string.tag);
        FriendsFragmentActivity=getActivity();
        friendArrList=new ArrayList<String>();
        friendArrListId=new ArrayList<String>();
        friendListView=(ListView)v.findViewById(R.id.friendListView);

    WebRTCDemo.mainActivity.runOnUiThread(new Runnable(){
        @Override
        public void run(){
            try {//first put json into arraylist, then use adapter the populate the listview
                Iterator<?> keys = LoginActivity.friendList.keys();
                while( keys.hasNext() ) {
                    String key = (String)keys.next();
                    friendArrListId.add(key.toString());
                    friendArrList.add(LoginActivity.friendList.getString(key));
                }

                adapter=new LazyAdapter(FriendsFragmentActivity,friendArrList);
                friendListView.setAdapter(adapter);

                friendListView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
                    @Override
                    public void onItemClick(AdapterView parentView, View childView, int position, long id){
                        Log.d("childview","Detected on click");
                        TextView t1=(TextView)childView.findViewById(R.id.userid);
                        String val=t1.getText().toString();
                        Toast.makeText(FriendsFragmentActivity.getApplicationContext(), val, Toast.LENGTH_SHORT).show();
                        Log.d("childview",val);
                        // TODO: 2015/10/21 open tab and chat, after able to update



                    }
                });


            }catch(JSONException jsonE){
                jsonE.printStackTrace();
                Log.d("JSONE","Json Exception");
            }

        }
    });

    return v;
  }


  @Override
  public void onAttach(Activity activity) {
    super.onAttach(activity);

    // This makes sure that the container activity has implemented
    // the callback interface. If not, it throws an exception.
    try {
      stateProvider = (MenuStateProvider) activity;
    } catch (ClassCastException e) {
      throw new ClassCastException(activity +
          " must implement MenuStateProvider");
    }
  }



  private String getLoopbackIPString() {
    return getResources().getString(R.string.loopbackIp);
  }

  private String getLocalIpAddress() {
    String localIp = "";
    try {
      for (Enumeration<NetworkInterface> en = NetworkInterface
               .getNetworkInterfaces(); en.hasMoreElements();) {
        NetworkInterface intf = en.nextElement();
        for (Enumeration<InetAddress> enumIpAddr =
                 intf.getInetAddresses();
             enumIpAddr.hasMoreElements(); ) {
          InetAddress inetAddress = enumIpAddr.nextElement();
          if (!inetAddress.isLoopbackAddress()) {
            // Set the remote ip address the same as
            // the local ip address of the last netif
            localIp = inetAddress.getHostAddress().toString();
          }
        }
      }
    } catch (SocketException e) {
      Log.e(TAG, "Unable to get local IP address. Not the end of the world", e);
    }
    return localIp;
  }


}