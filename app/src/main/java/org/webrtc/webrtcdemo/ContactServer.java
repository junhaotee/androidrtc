package org.webrtc.webrtcdemo;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;

/**
 * Created by junhao on 2015/10/26.
 */
public class ContactServer implements Runnable {
    public void run(){
        Log.d("junhao","Runnable started");
        getFriendList();
    }
    public void getFriendList(){

        try{
            JSONArray friendListRequest=new JSONArray();
            friendListRequest.put(0,3);
            while(true){
                Log.d("junhao","Sending friendlist request to server");
                LoginActivity.out.println(friendListRequest.toString());
                Thread.sleep(2000);
            }
        }catch(JSONException jsone){
            Log.d("junhao","ContactServer jsone triggered by getFriendsList");
            jsone.printStackTrace();
        }catch(InterruptedException ie){
            Log.d("junhao","ContactServer ie triggered by getFriendsList");
            ie.printStackTrace();
        }
    }



}
