package org.webrtc.webrtcdemo;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.util.Collections;

public class BackgroundTask implements Runnable {
    //background task listening to server response
    @Override
    public void run() {
        String serverResponse;
        JSONObject responseObj;
        try {//starts listening after logged in
            Log.d("junhao","background task started");
            while((serverResponse=LoginActivity.in.readLine())!=null){
                responseObj=new JSONObject(serverResponse.toString());
                Log.d("junhao","severResponse:"+responseObj.toString());
                switch(responseObj.getInt("0")){
                    case 13:{refreshFriendlist(responseObj);} //get an active friends list
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void refreshFriendlist(JSONObject responseObj){
       // Log.d("junhao",responseObj.toString());
    }
}
